<?php

namespace App;


use App\Profile;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Company;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function profile()
    {
        return $this->hasOne(Profile::class,'user_id');
    }
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class)->withTimeStamps();
    }

    public function favourites()
    {
        return $this->belongsToMany('App\Job','favourites','user_id','job_id')->withTimeStamps();
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
