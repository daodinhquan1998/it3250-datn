<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\Company;
use App\Http\Requests\EmployerRegister;
use Illuminate\Support\Facades\Hash;

class EmployerRegisterController extends Controller
{



    public function register(EmployerRegister $request)
    {
        $user = User::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'user_type'=> $request['user_type'],
            'name'=> $request['cname']
        ]);
        Company::create([
            'user_id'=>$user->id,
            'cname'=>request('cname'),
            'slug'=>str_slug(request('cname'))
        ]);
        $user->sendEmailVerificationNotification();
        return redirect()->back()->with('message','A verification has send to your mail.Please follow the link to verify it ');
    }
}
