<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\User;
use App\Company;
use App\Http\Requests\JobPostRequest;
use App\Category;
use App\Post;
use Illuminate\Support\Collection;
use Auth;

class JobController extends Controller
{
    public function __construct(){
        $this->middleware(['employer','verified'],['except'=>array('index','list','show','apply','allJob','searchJobs','favourties')]);
    }

    public function list()
    {
        $jobs = Job::latest()->limit(5)->where('status','1')->get();
        $company = Company::latest()->limit(5)->get();
        return view('jobs.list')->with(compact('jobs','company'));
    }
    public function index()
    {
        $jobs = Job::latest()->limit(5)->where('status','1')->get();
        $category = Category::with('jobs')->get();
        $posts = Post::where('status','1')->get();
        $companies = Company::get()->random(12);
        return view('welcome')->with(compact('jobs','category','posts','companies'));
    }

    public function show($id,Job $job)
    {
        $data = [];
        $jobBasedOnCategories = Job::latest()->where('category_id',$job->category_id)
            ->whereDate('last_date','>',date('Y-m-d'))
            ->where('id','!=',$job->id)
            ->where('status',1)
            ->get();
        array_push($data,$jobBasedOnCategories);

        $jobBasedOnCompany = Job::latest()
            ->where('company_id',$job->company_id)
            ->whereDate('last_date','>',date('Y-m-d'))
            ->where('id','!=',$job->id)
            ->where('status',1)
            ->get();
        array_push($data,$jobBasedOnCompany);
        $jobBasedOnPosition= Job::where('position','LIKE','%'.$job->position.'%')
            ->where('id','!=',$job->id)
            ->where('status',1)
            ->get();
        array_push($data,$jobBasedOnPosition);

        $jobRecommendations = $jobBasedOnCategories;


//        $collection  = collect($data);
//        $unique = $collection->unique('id');
//
//        $jobRecommendations =  $unique->values()->first();
        return view('jobs.show',with(compact('job','jobRecommendations')));
    }

    public function create()
    {

        return view('jobs.create');
    }

    public function store(JobPostRequest $request)
    {
        $user_id = auth()->user()->id;
        $company = Company::where('user_id',$user_id)->first();
        $company_id = $company->id;
        Job::create([
            'user_id'=>$user_id,
            'company_id'=>$company_id,
            'title'=>request('title'),
            'slug'=>str_slug(request('title')),
            'description'=>request('description'),
            'roles'=>request('roles'),
            'category_id'=>request('category'),
            'position'=>request('position'),
            'address'=>request('address'),
            'type'=>request('type'),
            'status'=>request('status'),
            'last_date'=>request('last_date'),
            'number_of_vacancy' => request('number_of_vacancy'),
            'gender' => request('gender'),
            'experience' => request('experience'),
            'salary' => request('salary')
        ]);
        return redirect()->back()->with('message','Job successfully created');
    }
    public function edit($id)
    {
        $job = Job::findOrFail($id);
        return view('jobs.edit',with(compact('job')));
    }
    public function update($id,Request $request)
    {
        $job = Job::findOrFail($id);
        $job->update($request->all());
        return redirect()->back()->with('message','Job successfully updated');

    }

    public function searchJobs(Request $request)
    {
        $keyword = $request->get('keyword');
        $job =  Job::where('title','like','%'.$keyword.'%')
            ->orWhere('position','like','%'.$keyword.'%')->limit(5)->get();
        return response()->json($job);
    }

    public function myJob()
    {
        $user_id = auth()->user()->id;
        $jobs = Job::where('user_id',$user_id)->get();
        return view('jobs.myjob',with(compact('jobs')));
    }

    public function allJob(Request $request)
    {
        $search = $request->get('search');
        $address = $request->get('address');
        if($search && $address)
        {
            $jobs = Job::where('position','LIKE','%'.$search.'%')
                ->orWhere('title','LIKE','%'.$search.'%')
                ->orWhere('type','LIKE','%'.$search.'%')
                ->orWhere('address','LIKE','%'.$address.'%')
                ->paginate(10);
            return view('jobs.all_job',with(compact('jobs')));
        }


        $keyword = $request->get('search');
        $type = $request->get('search');
        $category = $request->get('search');
        $address = $request->get('address');


//        $condition = [['title','LIKE','%'.$keyword.'%'],['type','=',$type],['category_id','=',$category],['address','LIKE','%'.$address.'%']];
//
//        if(Empty($condition)) {
//            $jobs = Job::latest()->paginate(10);
//            return view('jobs.all_job', with(compact('jobs')));
//
//        } else {
//            $jobs = Job::where($condition)->paginate(10);
//
//            return view('jobs.all_job', with(compact('jobs')));
//        }
        if($keyword || $category || $type || $address)
        {

            $jobs = Job::where('position','LIKE','%'.$keyword.'%')
                ->orWhere('type',$type)
                ->orWhere('category_id',$category)
                ->orWhere('address','LIKE','%'.$address.'%')
                ->paginate(10);
            return view('jobs.all_job',with(compact('jobs')));
        } else {
            $jobs = Job::latest()->paginate(10);
            return view('jobs.all_job',with(compact('jobs')));
        }
    }
    public function apply(Request $request,$id)
    {
        $jobId = Job::find($id);
        $jobId->users()->attach(auth()->user()->id);
        return redirect()->back()->with('message','Apply successfully ');

    }

    public function applicant()
    {
        $applicants = Job::with('users')->where('user_id',auth()->user()->id)->get();
        return view('jobs.applicant',with(compact('applicants')));
    }



}
