<?php

namespace App\Http\Controllers;

use App\Company;
use App\Job;
use App\Http\Requests\profilesRequest;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['employer','verified'],['except'=>array('index','list')]);
    }

    public function index($id,Company $company)
    {

        $jobs = Job::where('user_id',$id)->get();
        return view('company.index',compact('company'));
    }

    public function list()
    {
        $companies = Company::latest()->paginate(8);
        return view('company.list')->with(compact('companies'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $user_id = auth()->user()->id;

        Company::where('user_id',$user_id)->update([
            'address'=>request('address'),
            'phone'=>request('phone'),
            'website'=>request('website'),
            'description'=>request('description'),
            'slogan'=>request('slogan')
        ]);
        return redirect()->back()->with('message','Company information successfully updated');
    }

    public function coverphoto(Request $request)
    {
        $this->validate($request,[
            'cover_photo'=>'required | mimes:png,jpeg,jpg| max :20000'
        ]);
        $user_id = auth()->user()->id;
        if($request->hasFile('cover_photo'))
            $file = $request->file('cover_photo');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('uploads/cover_photo',$filename);
        Company::where('user_id',$user_id)->update([
            'cover_photo'=>$filename
        ]);
        return redirect()->back()->with('message','Cover photo successfully updated');
    }

    public function logo(Request $request)
    {
        $this->validate($request,[
            'logo'=>'required | mimes:png,jpeg,jpg| max :20000'
        ]);
        $user_id = auth()->user()->id;
        if($request->hasFile('logo'))
            $file = $request->file('logo');
        $ext = $file->getClientOriginalExtension();
        $filename = time().'.'.$ext;
        $file->move('uploads/logo',$filename);
        Company::where('user_id',$user_id)->update([
            'logo'=>$filename
        ]);
        return redirect()->back()->with('message','Logo successfully updated');
    }
}
