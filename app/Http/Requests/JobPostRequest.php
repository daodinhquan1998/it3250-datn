<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10',
            'roles' => 'required|min:5',
            'description' => 'required|min:10',
            'position' => 'required|min:5',
            'address'=>'required',
            'last_date'=>'required',
            'number_of_vacancy' => 'required | numeric',
            'experience' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.min' => 'The Title is too short',
            'roles.min' => 'The Role is too short',
            'description.min' => 'The Description is too short',
            'position.min' => 'The Position is too short',
            'number_of_vacancy.numeric' => 'The number of vacancy is number'
        ];
    }
}
