<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class profilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'bio' => 'required| min:5',
            'experience' => 'required| min:5',
         //   'phone_number' => 'required | regex: [/(01) [0-9] {9}]',
            'phone_number'=>'required'
        ];
    }
}
