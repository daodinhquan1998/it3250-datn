@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
           <h3>{{$categoryName->name}}</h3>
            <table class="table">
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td><img src="{{asset('avatar/avatar.jpg')}}" width="80px"></td>
                        <td>{{$job->position}}
                            <br>
                            <i class="fas fa-clock" aria-hidden="true"></i>&nbsp;{{$job->type}}
                        </td>
                        <td>{{str_limit($job->title,50)}}</td>
                        <td><i class="fas fa-map-marker-alt" aria-hidden="true"></i>&nbsp;{{$job->address}}</td>
                        <td><i class="fas fa-globe" aria-hidden="true"></i>&nbsp;{{$job->created_at->diffForHumans()}}</td>
                        <td>
                            <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                <button class="btn btn-success btn-sm">Apply</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$jobs->appends(Illuminate\Support\Facades\Request::except('page'))->links()}}
        </div>
    </div>

@endsection
<style>
    .fas{
        color: #2F4F4F;
    }
</style>
