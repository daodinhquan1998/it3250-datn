<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jobs </title>
    <script defer src="{{asset('js/app.js')}}"></script>
    @include('../partials/head')
</head>
<body>
    @include('../partials/nav')
    <br><br><br><br><br>
    @yield('content')
    <br><br><br><br><br>
    @include('../partials/footer')



</body>
</html>
