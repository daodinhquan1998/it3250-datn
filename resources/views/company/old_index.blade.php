@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <div class="company-profile">
                @if(!Empty($company->cover_photo))
                <img src="{{asset('uploads/cover_photo')}}/{{$company->cover_photo}}" style="width:100%">
                @else
                        <img src="{{asset('cover/banner')}}">
                @endif
                <div class="company-desc">

                    @if(!Empty($company->logo))
                        <img width="100" src="{{asset('uploads/logo')}}/{{$company->logo}}" >
                    @else
                        <img src="{{asset('avatar/avatar2.png')}}" width="100">
                    @endif
                    <p>{{$company->description}}</p>
                    <h1>{{$company->cname}}</h1>
                    <p><strong>Slogan</strong> {{$company->slogan}}&nbsp;
                    Address-{{$company->address}}&nbsp;
                    Phone-{{$company->phone}}&nbsp;
                    Website-{{$company->website}}&nbsp;
                    </p>
                </div>
            </div>
            <table class="table">
                <thead>
                <th>Logo</th>
                <th>Position</th>
                <th>Address</th>
                <th>Date</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($company->jobs as $job)
                    <tr>
                        <td><img src="{{asset('avatar/avatar.jpg')}}" width="80px"></td>
                        <td>{{$job->position}}
                            <br>
                            <i class="fas fa-clock" aria-hidden="true"></i>&nbsp;{{$job->type}}
                        </td>
                        <td><i class="fas fa-map-marker-alt" aria-hidden="true"></i>&nbsp;{{$job->address}}</td>
                        <td><i class="fas fa-globe" aria-hidden="true"></i>&nbsp;{{$job->created_at->diffForHumans()}}</td>
                        <td>
                            <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                <button class="btn btn-success btn-sm">Apply</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
