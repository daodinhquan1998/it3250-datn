@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @if(empty(Auth::user()->company->logo))
                    <img src="{{asset('avatar/avatar2.png')}}" width="200">
                @else
                    <img src="{{asset('uploads/logo')}}/{{Auth::user()->company->logo}}" width="100%" style="border: solid #808080 1px"
                    >
                @endif
                <form action="{{route('company.logo')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header" >Update Logo</div>
                        <div class="card-body">
                            <input type="file" class="form-control" name="logo">

                            <button style="margin-top:10px" class="btn btn-success float-right" type="submit">Update
                            </button>
                        </div>
                </form>
            </div>
        </div>

        <div class="col-md-5">
{{--            @if ($errors->any())--}}
{{--                <div class="alert alert-danger">--}}
{{--                    <ul>--}}
{{--                        @foreach ($errors->all() as $error)--}}
{{--                            <li>{{ $error }}</li>--}}
{{--                        @endforeach--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            @endif--}}
            <form action="{{route('company.store')}}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-header">Update your Company information</div>
                    <div class="card-body">

                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="address"
                                   value="{{Auth::user()->company->address}}">
                        </div>

                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="phone"
                                   value="{{Auth::user()->company->phone}}">
                        </div>

                        <div class="form-group">
                            <label for="">Website</label>
                            <input name="website" value="{{Auth::user()->company->website}}"
                                      class="form-control"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Slogan</label>
                            <input name="slogan" value="{{Auth::user()->company->slogan}}"
                                   class="form-control"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control">{{Auth::user()->company->description}}</textarea>
                        </div>



                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </div>
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{Session::get('message')}}
                    </div>
            @endif
        </div>

        </form>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">About company</div>
                <div class="card-body">
                    <p>Company name:{{Auth::user()->company->cname}}</p>
                    <p>Address:{{Auth::user()->company->address}}</p>
                    <p>Phone number:{{Auth::user()->company->phone}}</p>
                    <p>Website:
                        <a href="#">{{Auth::user()->company->website}}
                    </a>
                    </p>
                    <p>Description:{{Auth::user()->company->description}}</p>
                    <p>Company page:<a href="{{Auth::user()->company->user_id}}/{{Auth::user()->company->slug}}">View</a></p>
                </div>
            </div>

            <form action="{{route('company.coverphoto')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">Update coverphoto</div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="cover_photo">

                        <button style="margin-top:10px" class="btn btn-success float-right" type="submit">Update
                        </button>
                    </div>
            </form>

        </div>


    </div>
    </div>
    </div>
@endsection
