@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Recent Jobs</h1>
            <table class="table">
                <thead>
                <th>Logo</th>
                <th>Position</th>
                <th>Address</th>
                <th>Date</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td>
                        @if(empty(Auth::user()->company->logo))
                            <img src="{{asset('avatar/avatar2.png')}}" width="100">
                        @else
                            <img src="{{asset('uploads/logo')}}/{{Auth::user()->company->logo}}" width="100">
                        @endif
                        </td>
                        <td>{{$job->position}}
                            <br>
                            <i class="fas fa-clock" aria-hidden="true"></i>&nbsp;{{$job->type}}
                        </td>
                        <td><i class="fas fa-map-marker-alt" aria-hidden="true"></i>&nbsp;{{$job->address}}</td>
                        <td><i class="fas fa-globe" aria-hidden="true"></i>&nbsp;{{$job->created_at->diffForHumans()}}</td>
                        <td>
                                <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                <button class="btn btn-success btn-sm" >Read</button>
                            </a>
                            <br><br>
                            <a href="{{route('jobs.edit',[$job->id])}}">
                                <button class="btn btn-dark ">Edit</button>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
<style>
    .fas{
        color: #4183D7;
    }
</style>
