@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{Session::get('message')}}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{  __('Some thing wrong, please check text box ') }}</li>
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Create job</div>

                    <div class="card-body">
                        <form action="{{route('jobs.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" name="title"
                                       class="form-control @error('title') is-invalid @enderror"
                                       value="{{ old('title') }}" required autocomplete="title">
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category" class="form-control">
                                    @foreach(App\Category::all() as $cate)
                                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="role">Benefit</label>
                                <textarea type="text" name="roles"
                                          class="form-control @error('roles') is-invalid @enderror"
                                          required autocomplete="role">{{ old('roles') }}</textarea>
                                @error('roles')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea type="text" name="description"
                                          class="form-control @error('description') is-invalid @enderror"
                                          required autocomplete="description">{{ old('description') }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <input type="text" name="position"
                                       class="form-control @error('position') is-invalid @enderror"
                                       value="{{ old('position') }}" required autocomplete="position">

                                @error('position')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" name="address"
                                       class="form-control @error('address') is-invalid @enderror"
                                       value="{{ old('address') }}" required autocomplete="address">

                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="number_of_vacancy">No of vacancy:</label>
                                <input type="text" name="number_of_vacancy"
                                       class="form-control @error('number_of_vacancy') is-invalid @enderror"
                                       value="{{ old('number_of_vacancy') }}" required autocomplete="number_of_vacancy">

                                @error('number_of_vacancy')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="experience">Year of experience:</label>
                                <input type="text" name="experience"
                                       class="form-control @error('experience') is-invalid @enderror"
                                       value="{{ old('experience') }}" required autocomplete="experience">

                                @error('experience')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="type">Gender:</label>
                                <select class="form-control" name="gender">
                                    <option value="any">Any</option>
                                    <option value="male">male</option>
                                    <option value="female">female</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="type">Salary/year:</label>
                                <select class="form-control" name="salary">
                                    <option value="negotiable">Negotiable</option>
                                    <option value="200-500">200 - 500 USD</option>
                                    <option value="500-1000">500 - 1000 USD</option>
                                    <option value="1000-2000">1000 - 2000 USD</option>
                                    <option value="3000-4000">3000 - 5000 USD</option>
                                    <option value="4000-5000">4000 - 5000 USD</option>

                                    <option value="5000+">5000+ USD</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="type">Type</label>
                                <select class="form-control" name="type">
                                    <option value="fulltime">full-time</option>
                                    <option value="parttime">part-time</option>
                                    <option value="collaborators">collaborator</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="type">Status</label>
                                <select class="form-control" name="status">
                                    <option value="1">Live</option>
                                    <option value="0">Draft</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="last_date">Last date</label>
                                <input type="date" name="last_date"
                                       class="form-control @error('last_date') is-invalid @enderror"
                                       value="{{ old('last_date') }}" required autocomplete="last_date">
                                @error('last_date')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-dark">Submit</button>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
