@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <br>
            <br>
            <h1>Recent Jobs</h1>
            <div class="col-md-12">
                <search-component></search-component>
            </div>
            <table class="table">
                <thead>
                <th>Logo</th>
                <th>Position</th>
                <th>Title</th>
                <th>Address</th>
                <th>Date</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                <tr>
                    <td><img src="{{asset('avatar/avatar.jpg')}}" width="80px"></td>
                    <td>{{$job->position}}
                    <br>
                    <i class="fas fa-clock" aria-hidden="true"></i>&nbsp;{{$job->type}}
                    </td>
                    <td>{{str_limit($job->title,50)}}</td>
                    <td><i class="fas fa-map-marker-alt" aria-hidden="true"></i>&nbsp;{{$job->address}}</td>
                    <td><i class="fas fa-globe" aria-hidden="true"></i>&nbsp;{{$job->created_at->diffForHumans()}}</td>
                    <td>
                        <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                        <button class="btn btn-success btn-sm">Apply</button>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <a href="{{route('jobs.alljob')}}">
            <button class="btn btn-success btn-lg" style="width: 100%;"> Browse all jobs </button></a>
        </div>
        <br><br>
        <h1>Feature Companies</h1>
    </div>
    <div class="container">
        <div class="row">
            @foreach($company as $company)
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="{{asset('uploads/logo')}}/{{$company->logo}}" style="width:120">
                    <div class="card-body">
                        <h5 class="card-title">{{$company->cname}}</h5>
                        <p class="card-text">{{str_limit($company->description,20)}}</p>
                        <a href="{{route('company.index',[$company->id,$company->slug])}}" class="btn btn-primary">Visit Company</a>
                    </div>
                </div>
            </div>
                @endforeach
        </div>
    </div>
@endsection
<style>
    .fas{
        color: #2F4F4F;
    }
</style>
