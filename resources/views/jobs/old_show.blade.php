@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$job->title}}</div>

                    <div class="card-body">
                        <p>
                        <h3>Description</h3>
                        {{$job->description}}
                        </p>
                        <p>
                        <h3>Duties</h3>
                        {{$job->roles}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Short info</div>

                    <div class="card-body">
                        <p>Company:
                            <a href="{{route('company.index',[$job->id,$job->company->slug])}}">
                            {{$job->company->cname}}
                            </a></p>
                        <p>Address: {{$job->address}}</p>
                        <p>Employment Type: {{$job->type}}</p>
                        <p>Position: {{$job->position}}</p>
                        <p>Date: {{$job->created_at->diffForHumans()}}</p>
                        <p>Late date for apply: {{date('F d,Y',strtotime($job->last_date))}}</p>
                    </div>
                </div>
                <br>
                @if(Auth::check() && Auth::user()->user_type='seeker')
                    @if(!$job->checkApplication())
{{--                    <form action="{{route('jobs.apply',[$job->id])}}" method="POST">--}}
{{--                        @csrf--}}
{{--                    <button class="btn btn-success" style="width: 100%">Applsssy</button>--}}
{{--                    </form>--}}
                        <apply-component :jobid={{$job->id}}></apply-component>
                    @endif
                <br>
                    <favourites-component :jobid={{$job->id}} :favouited={{$job->checkSaved()?'true':'false'}}></favourites-component>
                @endif
            </div>
        </div>
    </div>
@endsection
<script>
    import FavouriteComponent from "../../js/components/FavouriteComponent";
    export default {
        components: {FavouriteComponent}
    }
</script>
