@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <form action="{{route('jobs.alljob')}}" method="GET">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Keyword&nbsp;</label>
                        <input type="text" name="keyword" class="form-control">&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <label>Type &nbsp;</label>
                        <select class="form-control" name="type">
                            <option value="">-select-</option>
                            <option value="fulltime">full-time</option>
                            <option value="parttime">part-time</option>
                            <option value="collaborators">collaborator</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label>Category&nbsp;</label>
                        <select name="category_id" class="form-control">
                            <option value="">-select-</option>
                            @foreach(App\Category::all() as $cate)
                                <option value="{{$cate->id}}">{{$cate->name}}</option>
                            @endforeach
                        </select>&nbsp;
                    </div>
                    <div class="form-group">
                        <label>Address&nbsp;</label>
                        <input type="text" name="address" class="form-control">
                    </div>
                    &nbsp;
                    <div class="form-group">

                        <button type="submit" class="btn btn-warning btn-lg">Search
                    </div>
                </div>
            </form>
            <div class="col-md-12">
            <div class="rounded border jobs-wrap">
                @foreach($jobs as $job)
                    <a href="{{route('jobs.show',[$job->id,$job->slug])}}"
                       class="job-item d-block d-md-flex align-items-center  border-bottom fulltime">
                        <div class="company-logo blank-logo text-center text-md-left pl-3">
                            <img src="{{asset('uploads/logo')}}/{{$job->company->logo}}" alt="Image" class="img-fluid mx-auto">
                        </div>
                        <div class="job-details h-100">
                            <div class="p-3 align-self-center">
                                <h3>{{$job->position}}</h3>
                                <div class="d-block d-lg-flex">
                                    <div class="mr-3"><span class="icon-suitcase mr-1"></span>{{$job->company->cname}}</div>
                                    <div class="mr-3"><span class="icon-room mr-1"></span>{{str_limit($job->address,20)}}</div>
                                    <div><span class="icon-money mr-1"></span> {{$job->salary}}$</div>&nbsp;&nbsp;&nbsp;
                                    <div><span class="fas fa-clock mr-1"></span>{{$job->created_at->diffForHumans()}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="job-category align-self-center">
                            @if($job->type == 'fulltime')
                                <div class="p-3">
                                    <span class="text-info p-2 rounded border border-info">Full Time</span>
                                </div>
                            @elseif($job->type == 'parttime')
                                <div class="p-3">
                                    <span class="text-warning p-2 rounded border border-warning">Part Time</span>
                                </div>
                            @else
                                <div class="p-3">
                                    <span class="text-danger p-2 rounded border border-danger">Collaborators</span>
                                </div>
                            @endif
                        </div>
                    </a>
                @endforeach


            </div>
            </div>

            <br>
            {{$jobs->appends(Illuminate\Support\Facades\Request::except('page'))->links()}}
        </div>
    </div>

@endsection
<style>
    .fas{
        color: #2F4F4F;
    }
</style>
