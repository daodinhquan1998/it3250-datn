@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form action="{{route('jobs.alljob')}}" method="GET">
            <div class="form-inline">
                <div class="form-group">
                    <label>Keyword&nbsp;</label>
                    <input type="text" name="keyword" class="form-control">&nbsp;&nbsp;&nbsp;
                </div>
                <div class="form-group">
                    <label>Employment type&nbsp;</label>
                    <select class="form-control" name="type">
                        <option value="">-select-</option>
                        <option value="fulltime">full-time</option>
                        <option value="parttime">part-time</option>
                        <option value="collaborators">collaborator</option>
                    </select>
                    &nbsp;&nbsp;
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category_id" class="form-control">
                        <option value="">-select-</option>
                    @foreach(App\Category::all() as $cate)
                            <option value="{{$cate->id}}">{{$cate->name}}</option>
                        @endforeach
                    </select>&nbsp;&nbsp;
                </div>
                <div class="form-group">
                    <label>Address&nbsp;</label>
                    <input type="text" name="address" class="form-control">&nbsp;&nbsp;&nbsp;
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-outline-success">Search
                </div>
            </div>
            </form>

            <table class="table">
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td><img src="{{asset('avatar/avatar.jpg')}}" width="80px"></td>
                        <td>{{$job->position}}
                            <br>
                            <i class="fas fa-clock" aria-hidden="true"></i>&nbsp;{{$job->type}}
                        </td>
                        <td>{{str_limit($job->title,50)}}</td>
                        <td><i class="fas fa-map-marker-alt" aria-hidden="true"></i>&nbsp;{{$job->address}}</td>
                        <td><i class="fas fa-globe" aria-hidden="true"></i>&nbsp;{{$job->created_at->diffForHumans()}}</td>
                        <td>
                            <a href="{{route('jobs.show',[$job->id,$job->slug])}}">
                                <button class="btn btn-success btn-sm">Apply</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$jobs->appends(Illuminate\Support\Facades\Request::except('page'))->links()}}
        </div>
    </div>

@endsection
<style>
    .fas{
        color: #2F4F4F;
    }
</style>
