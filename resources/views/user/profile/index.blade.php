@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @if(empty(Auth::user()->profile->avatar))
                <img src="{{asset('avatar/avatar2.png')}}" style="width: 100%">
                @else
                    <img src="{{asset('uploads/avatar')}}/{{Auth::user()->profile->avatar}}" style="width: 100%">
                @endif
                <form action="{{route('user.profile.avatar')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">Update avatar</div>
                        <div class="card-body">
                            <input type="file" class="form-control" name="avatar">

                            <button style="margin-top:10px" class="btn btn-success float-right" type="submit">Update
                            </button>
                        </div>
                </form>
            </div>
        </div>

            <div class="col-md-5">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('user.profile.create')}}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header">Update your Profile</div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" name="name"
                                       value="{{Auth::user()->name}}">
                            </div>

                            <div class="form-group">
                                <label for="">Address</label>
                                <input type="text" class="form-control" name="address"
                                       value="{{Auth::user()->profile->address}}">
                            </div>

                            <div class="form-group">
                                <label for="">Phone number</label>
                                <input type="text" class="form-control" name="phone_number"
                                       value="{{Auth::user()->profile->phone_number}}">
                            </div>

                            <div class="form-group">
                                <label for="">Experience</label>
                                <textarea name="experience"
                                          class="form-control">{{Auth::user()->profile->experience}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Bio</label>
                                <textarea name="bio" class="form-control">{{Auth::user()->profile->bio}}</textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </div>
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{Session::get('message')}}
                        </div>
                @endif
            </div>

            </form>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">About you</div>
                    <div class="card-body">
                        <p>Name: {{Auth::user()->name}}</p>
                        <p>Email: {{Auth::user()->email}}</p>
                        <p>Address: {{Auth::user()->profile->address}}</p>
                        <p>Phone: {{Auth::user()->profile->phone_number}}</p>
                        <p>Gender: {{Auth::user()->profile->gender}}</p>
                        <p>Experience: {{Auth::user()->profile->experience}}</p>
                        <p>Bio: {{Auth::user()->profile->bio}}</p>
                        <p>Member On: {{date('F d Y',strtotime(Auth::user()->created_at))}}</p>

                        @if(!empty(Auth::user()->profile->cover_letter))
                            <p><a href="{{Storage::url(Auth::user()->profile->cover_letter)}}">
                                    Cover letter
                                </a>
                            </p>
                         @else
                            <p>Please upload cover letter</p>
                            @endif

                        @if(!empty(Auth::user()->profile->resume))
                            <p><a href="{{Storage::url(Auth::user()->profile->resume)}}">
                                   Resume
                                </a>
                            </p>
                        @else
                            <p>Please upload Resume</p>
                        @endif
                    </div>
                </div>

                <form action="{{route('user.profile.coverletter')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">Update Cover Letter</div>
                        <div class="card-body">
                            <input type="file" class="form-control" value="" name="cover_letter">

                            <button style="margin-top:10px" class="btn btn-success float-right" type="submit">Update
                            </button>
                        </div>
                </form>
                <form action="{{route('user.profile.resume')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">Update resume</div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="resume">
                        <button style="margin-top:10px" class="btn btn-success float-right" type="submit">Update
                        </button>
                    </div>
                </form>
            </div>


        </div>
    </div>
    </div>
@endsection
