<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Role;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   Category::truncate();
        factory('App\User',20)->create();
        factory('App\Company',20)->create();
        factory('App\Job',20)->create();

        $category = [
            'Techonogy',
            'Engineering',
            'Goverment',
            'Medical',
            'Construction',
            'Software',
        ];
        foreach($category as $category) {
            Category::create(['name'=>$category]);
        }
        Role::truncate();
        $adminRole = Role::create(['name'=>'admin']);
        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password'=>bcrypt('quan1998'),
            'email_verified_at' => NOW()
        ]);
        $admin->roles()->attach($adminRole);

    }
}
