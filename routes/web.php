<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//jobs
//Route::get('/jobs/list', 'JobController@list')->name('jobs.list');

Route::get('/','JobController@index');
Route::get('/jobs/{id}/{job}','JobController@show')->name('jobs.show');

Route::get('/job/{id}/edit','JobController@edit')->name('jobs.edit');
Route::post('/job/{id}/edit','JobController@update')->name('jobs.update');

Route::get('/jobs/create','JobController@create')->name('jobs.create');
Route::post('/jobs/store','JobController@store')->name('jobs.store');
Route::get('/jobs/myjob','JobController@myJob')->name('jobs.myjob');

Route::post('/applications/{id}','JobController@apply')->name('jobs.apply');
Route::get('/jobs/applications','JobController@applicant')->name('jobs.applicant');
Route::get('/jobs/allJob','JobController@allJob')->name('jobs.alljob');

Route::get('jobs/search','JobController@searchJobs');

Route::post('/save/{id}','FavouriteController@saveJob');
Route::post('/unsave/{id}','FavouriteController@unSaveJob');

//company
Route::get('/company/{id}/{company}','CompanyController@index')->name('company.index');
Route::get('/company/create','CompanyController@create')->name('company.create');
Route::post('/company/store','CompanyController@store')->name('company.store');
Route::post('/company/coverphoto','CompanyController@coverphoto')->name('company.coverphoto');
Route::post('/company/logo','CompanyController@logo')->name('company.logo');
Route::get('/company/list','CompanyController@list')->name('company.list');

//category
Route::get('/category/{id}','CategoryController@index')->name('category.index');

//user
Route::get('user/profile','UserController@index')->name('user.index');
Route::post('user/profile/create','UserController@store')->name('user.profile.create');
Route::post('user/profile/coverletter','UserController@coverletter')->name('user.profile.coverletter');
Route::post('user/profile/resume','UserController@resume')->name('user.profile.resume');
Route::post('user/profile/avatar','UserController@avatar')->name('user.profile.avatar');

//employer
Route::view('employer/register','auth.employer-register')->name('employer.register');
Route::post('employer/register','EmployerRegisterController@register')->name('emp.register');

//email
Route::post('/jobs/email','EmailController@sendJob')->name('email');
//Auth::routes();


Route::group(['prefix' => 'dashboard', 'middleware' => 'admin'],function (){
    Route::get('/','DashboardController@index')->name('dashboard');
    Route::get('/create','DashboardController@create');
    Route::post('/create','DashboardController@store')->name('post.store');
    Route::post('/destroy','DashboardController@destroy')->name('post.delete');

    Route::get('/{id}/edit','DashboardController@edit')->name('post.edit');
    Route::post('/{id}/update','DashboardController@update')->name('post.update');

    Route::get('/trash','DashboardController@trash')->name('post.trash');
    Route::get('/{id}/restore','DashboardController@restore')->name('post.restore');

    Route::get('/{id}/toggle','DashBoardController@toggle')->name('post.toggle');

    Route::get('/jobs','DashBoardController@getAllJobs')->name('post.addJob');
    Route::get('/{id}/statusJob','DashBoardController@changeJobStatus')->name('job.status');
});

Route::get('/{id}/{slug}/show','DashBoardController@show')->name('post.show');
//Route::get('/dashboard','DashboardController@index')->middleware('admin');
//Route::get('/dashboard/create','DashboardController@create')->middleware('admin');
//Route::post('/dashboard/create','DashboardController@store')->name('post.store')->middleware('admin');
//Route::post('/dashboard/destroy','DashboardController@destroy')->name('post.delete')->middleware('admin');
//
//Route::get('/dashboard/{id}/edit','DashboardController@edit')->name('post.edit')->middleware('admin');
//Route::post('/dashboard/{id}/update','DashboardController@update')->name('post.update')->middleware('admin');
//

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware();
Route::get('/favourites', 'UserController@favourties')->name('favourties');
